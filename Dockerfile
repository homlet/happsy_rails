FROM ruby:2.3.1

RUN apt-get update -qq && apt-get install -y curl
RUN curl --silent --location https://deb.nodesource.com/setup_8.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update -qq
RUN apt-get install -y nodejs yarn build-essential

RUN mkdir /happsy_rails
WORKDIR /happsy_rails

# Webpack
COPY package.json package.json

# Rails
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install --jobs 3 --no-binstubs

EXPOSE 8080

CMD bash -c "bin/rails db:create db:migrate db:seed && bin/rake"
