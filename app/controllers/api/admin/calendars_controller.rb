class Api::Admin::CalendarsController < Api::Admin::ApplicationController
  def create
    calendar = Calendar.new(calendar_params)

    if calendar.save
      render json: calendar, status: :created
    else
      render json: { errors: calendar.errors }, status: :bad_request
    end

  end

  def destroy
    event = Calendar.find(params[:id])
    event.destroy

    render :json => event
  end

  private

  def calendar_params
    params[:calendar].permit(:date)
  end
end