class Api::Admin::GalleriesController < Api::Admin::ApplicationController
  def create
    gallery = Gallery.new(gallery_params)
    cover_id = params[:cover_id]

    puts(cover_id)

    if gallery.save
      # cover_picture = Picture.find_by()
      render json: gallery, status: :created
    else
      render json: { errors: gallery.errors }, status: :bad_request
    end
  end

  def update
    gallery = Gallery.friendly.find(params[:id])
    gallery.assign_attributes(gallery_params)

    if gallery.save
      render json: gallery, status: :created
    else
      render json: { errors: gallery.errors }, status: :bad_request
    end
  end

  def destroy
    gallery = Gallery.friendly.find(params[:id])
    gallery.destroy

    respond_with gallery
  end

  private

  def gallery_params
    params.require(:gallery).permit(
        :title, :slug, :cover, category_ids:[],
        pictures_attributes: [:id, :title, :image, :filename, :size, :_destroy,
        :attachments_attributes => [:id, :gallery_id, :picture_id, :order]]
    )
  end
end