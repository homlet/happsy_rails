class Api::Admin::PicturesController < Api::Admin::ApplicationController
  def create
    @picture = Picture.new(picture_params)
    @picture.save

    render :json => @picture
  end

  def save_favorite
    picture = Picture.find(params[:picture_id])
    picture.favorite = true

    if picture.save
      render json: picture, status: :ok
    else
      render json: { errors: picture.errors }, status: :bad_request
    end
  end

  def delete_favorite
    picture = Picture.find(params[:picture_id])
    picture.favorite = false

    if picture.save
      render json: picture, status: :ok
    else
      render json: { errors: picture.errors }, status: :bad_request
    end
  end

  def picture_params
    params[:picture].permit(:title, :image, :filename, :size)
  end
end