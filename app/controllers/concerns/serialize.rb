module Concerns::Serialize
  def serialize(resource, options = {})
    return unless resource
    ActiveModel::SerializableResource.new(resource, options).serializable_hash
  end
end