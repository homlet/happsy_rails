class Web::Admin::ApplicationController < Web::ApplicationController
  before_action :require_login

  private
  def not_authenticated
    redirect_to new_admin_session_path
  end
end
