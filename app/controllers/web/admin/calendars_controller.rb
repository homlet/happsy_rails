class Web::Admin::CalendarsController < Web::Admin::ApplicationController
  def index
    @events = Calendar.all
    @events_serialized = serialize(@events)

    gon.CalendarReducer = {
        events: @events_serialized
    }
  end
end
