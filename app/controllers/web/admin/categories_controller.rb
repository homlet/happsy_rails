class Web::Admin::CategoriesController < Web::Admin::ApplicationController
  def index
    @q = Category.ransack(params[:q])
    @categories = @q.result.order(created_at: :desc).page(params[:page])
  end

  def new
    @category = Category.new
  end

  def edit
    @category = Category.friendly.find(params[:id])
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      f(:success)
      redirect_to admin_categories_path
    else
      f(:error)
      render :new
    end
  end

  def update
    @category = Category.friendly.find(params[:id])
    @category.assign_attributes(category_params)

    if @category.save
      f(:success)
      redirect_to edit_admin_category_path(@category)
    else
      f(:error)
      render :edit
    end
  end

  def destroy
    category = Category.friendly.find(params[:id])
    category.destroy
    f :success
    redirect_to action: :index
  end

  private

  def category_params
    params.require(:category).permit(:title)
  end

end