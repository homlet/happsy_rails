class Web::Admin::GalleriesController < Web::Admin::ApplicationController
  def index
    @q = Gallery.ransack(params[:q])
    @galleries = @q.result.order(created_at: :desc).page(params[:page])
  end

  def new
    @gallery = Gallery.new
    @categories = Category.all
    @categories_serialized = serialize(@categories)
  end

  def edit
    @gallery = Gallery.friendly.find(params[:id])
    @gallery_serialized = serialize(@gallery)
    @pictures_serialized = serialize(@gallery.pictures)
    @attachments_serialized = serialize(@gallery.attachments)
    @categories = Category.all
    @categories_serialized = serialize(@categories)
  end

  def destroy
    gallery = Gallery.friendly.find(params[:id])
    gallery.destroy
    f :success
    redirect_to action: :index
  end

end