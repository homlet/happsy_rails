class Web::Admin::PagesController < Web::Admin::ApplicationController
  def index
    @q = Page.ransack(params[:q])
    @pages = @q.result.order(created_at: :desc).page(params[:page])
  end

  def new
    @page = Page.new
  end

  def create
    @page = Page.new(page_params)

    if @page.save
      f(:success)
      redirect_to admin_pages_path
    else
      f(:error)
      render :new
    end
  end

  def update
    @page = Page.friendly.find(params[:id])
    @page.assign_attributes(page_params)

    if @page.save
      f(:success)
      redirect_to edit_admin_page_path(@page)
    else
      f(:error)
      render :edit
    end
  end

  def edit
    @page = Page.friendly.find(params[:id])
  end

  def destroy
    page = Page.friendly.find(params[:id])
    page.destroy
    f :success
    redirect_to action: :index
  end

  private

  def page_params
    params.require(:page).permit(:title, :body)
  end
end