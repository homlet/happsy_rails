class Web::Admin::PicturesController < Web::Admin::ApplicationController
  def index
    @q = Picture.ransack(params[:q])
    @pictures = @q.result.order(created_at: :desc).page(params[:page])
  end

  def new
    @picture = Picture.new
  end

  def edit
    @picture = Picture.find(params[:id])
  end

  def update
    @picture = Picture.find(params[:id])
    @picture.assign_attributes(picture_params)

    if @picture.save
      f(:success)
      redirect_to edit_admin_picture_path(@picture)
    else
      f(:error)
      render :edit
    end
  end

  def destroy
    picture = Picture.find(params[:id])
    picture.destroy
    f :success
    redirect_to action: :index
  end

  private

  def picture_params
    params.require(:picture).permit(:title, :image)
  end
end