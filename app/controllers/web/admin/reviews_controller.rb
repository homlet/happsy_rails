class Web::Admin::ReviewsController < Web::Admin::ApplicationController
  def index
    @q = Review.ransack(params[:q])
    @reviews = @q.result.order(created_at: :desc).page(params[:page])
  end

  def new
    @review = Review.new
  end

  def edit
    @review = Review.find(params[:id])
  end

  def create
    @review = Review.new(review_params)

    if @review.save
      f(:success)
      redirect_to admin_reviews_path
    else
      f(:error)
      render :new
    end
  end

  def update
    @review = Review.find(params[:id])

    @review.assign_attributes(review_params)
    
    if @review.save
      f(:success)
      redirect_to edit_admin_review_path(@review)
    else
      f(:error)
      render :edit
    end
  end

  def destroy
    review = Review.find(params[:id])
    review.destroy
    f :success
    redirect_to action: :index
  end

  private

  def review_params
    params.require(:review).permit(:title, :text, :image)
  end
end
