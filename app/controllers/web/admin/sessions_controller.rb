class Web::Admin::SessionsController < Web::Admin::ApplicationController
  skip_before_action :require_login, only: [:new, :create]

  layout 'web/application'

  def new
    @user = User.new
  end

  def create
    if @user = login(session_params[:email], session_params[:password])
      redirect_to admin_root_path
    else
      @user = User.new
      f(:user_or_password_invalid, now: true, type: :error)
      render :new
    end
  end

  def destroy
    logout
    redirect_to root_path
  end

  private

  def session_params
    params.require(:user)
  end
end
