class Web::Admin::WelcomeController < Web::Admin::ApplicationController
  def index
    @q = Picture.ransack(params[:q])
    @pictures = @q.result.order(:favorite).page(params[:page])
    @pictures_serialized = serialize(@pictures)

    gon.FavoriteReducer = {
      pictures: @pictures_serialized
    }
  end
end