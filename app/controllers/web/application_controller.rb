class Web::ApplicationController < ApplicationController
  include Concerns::FlashHelper
  include Concerns::Serialize
end
