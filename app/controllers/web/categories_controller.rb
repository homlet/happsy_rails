class Web::CategoriesController < Web::ApplicationController
  def show
    @category = Category.friendly.find(params[:id])
    @galleries = @category.galleries
  end
end