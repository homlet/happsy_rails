class Web::GalleriesController < Web::ApplicationController
  def show
    @gallery = Gallery.friendly.find(params[:id])
    @categories = @gallery.categories
    @pictures = @gallery.pictures
    @pictures_serialized = serialize(@pictures)
  end
end