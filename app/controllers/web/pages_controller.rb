class Web::PagesController < Web::ApplicationController
  def show
    @page = Page.friendly.find(params[:id])
  end

  def servise
    @servise = Page.find_by(slug: 'uslugi')
    @events = Calendar.all
    @events_serialized = serialize(@events)
  end

  def about_me
    @about_me = Page.find_by(slug: 'kontakty')
  end
end