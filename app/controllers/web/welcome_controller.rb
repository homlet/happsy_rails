class Web::WelcomeController < Web::ApplicationController
  def index
    @favorites = Picture.where(favorite: true)
    @favorites_serialized = serialize(@favorites)
  end
end