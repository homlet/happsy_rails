class Calendar < ActiveRecord::Base
  validates :date, uniqueness: true, presence: true
end
