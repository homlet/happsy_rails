class Category < ActiveRecord::Base
  extend FriendlyId

  has_and_belongs_to_many :galleries

  friendly_id :title, use: :slugged
end
