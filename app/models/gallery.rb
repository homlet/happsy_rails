class Gallery < ActiveRecord::Base
  extend FriendlyId
  # validates :slug, slug: true, presence: true, uniqueness: true
  
  has_many :pictures, through: :attachments
  has_many :attachments, dependent: :destroy
  has_and_belongs_to_many :categories

  attachment :cover, type: :image
  friendly_id :title, use: :slugged

  accepts_nested_attributes_for :pictures, allow_destroy: true
end
