class Picture < ActiveRecord::Base
  has_many :galleries, through: :attachments
  has_many :attachments, dependent: :destroy

  extend Enumerize

  attachment :image, type: :image
  enumerize :publish_state, in: [:active, :deleted], default: :active

  accepts_nested_attributes_for :attachments
end
