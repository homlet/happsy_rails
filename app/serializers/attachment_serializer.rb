class AttachmentSerializer < ActiveModel::Serializer
  attributes :id, :gallery_id, :picture_id, :order
end
