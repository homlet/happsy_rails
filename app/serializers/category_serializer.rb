class CategorySerializer < ActiveModel::Serializer
  attributes :id, :title, :slug

end
