class GallerySerializer < ActiveModel::Serializer
  include Refile::AttachmentHelper
  has_many :pictures
  has_many :categories

  attributes :id, :title, :slug, :cover_path

  def cover_path
    attachment_url(object, :cover, :limit, 500, 500, format: 'jpg')
  end
end
