class PictureSerializer < ActiveModel::Serializer
  include Refile::AttachmentHelper
  has_many :attachments
  attributes :id, :title, :favorite, :image_path, :filename, :size, :publish_state


  def image_path
    attachment_url(object, :image, :limit, 1600, 1000, format: 'jpg')
  end

  def medium_image_path
    attachment_url(object, :image, :limit, 1000, 1000, format: 'jpg')
  end
end
