import CalendarTypes from '../constants/CalendarTypes';
import getHttpParams from 'lib/utils/getHttpParams';
import Routes from 'routes';

export function changePickerDate(pickerDate) {
  return {
    type: CalendarTypes.CHANGE_PICKER_DATE,
    pickerDate
  };
}

export function saveEventSuccess(event) {
  return {
    type: CalendarTypes.SAVE_EVENT_SUCCESS,
    event
  };
}

export function deleteEventSuccess(event) {
  return {
    type: CalendarTypes.DELETE_EVENT_SUCCESS,
    event
  };
}

export function saveEvent(date) {
  const params = JSON.stringify({calendar: { date: date }});
  return (dispatch) => {
    return fetch(Routes.api_admin_calendars_path(), getHttpParams('POST', params))
      .then(res => res.json())
      .then((event) => {
        dispatch(saveEventSuccess(event));
      });
  };
}

export function deleteEvent(date) {
  return (dispatch) => {
    return fetch(Routes.api_admin_calendar_path(date.id), getHttpParams('DELETE'))
      .then(res => res.json())
      .then((event) => {
        dispatch(deleteEventSuccess(event));
      });
  };
}