import FavoriteTypes from '../constants/FavoriteTypes';
import Routes from 'routes';

export function saveFavoritesStart() {

  return { type: FavoriteTypes.SAVE_FAVORITE_START };
}

export function saveFavoritesSuccess(picture) {
  return { type: FavoriteTypes.SAVE_FAVORITE_SUCCESS, picture };
}

export function saveFavoritesFail() {
  return { type: FavoriteTypes.SAVE_FAVORITE_START };
}

export function deleteFavoritesStart() {
  return { type: FavoriteTypes.DELETE_FAVORITE_START };
}

export function deleteFavoritesSuccess(picture) {
  return { type: FavoriteTypes.DELETE_FAVORITE_SUCCESS, picture };
}

export function deleteFavoritesFail() {
  return { type: FavoriteTypes.DELETE_FAVORITE_FAIL };
}


export function saveFavorites(favoriteItem) {

  return (dispatch) => {
    dispatch(saveFavoritesStart());
    fetch(Routes.api_admin_picture_save_favorite_path(favoriteItem.id), {
      method: 'POST',
      credentials: 'include',
      headers: {
        'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').content
      }
    })
    .then(res => res.json())
    .then((favorites) => {
      dispatch(saveFavoritesSuccess(favorites));
    })
    .catch(() => {
      dispatch(saveFavoritesFail());
    });
  };
}

export function deleteFavorites(favoriteItem) {
  return (dispatch) => {
    dispatch(deleteFavoritesStart());

    fetch(Routes.api_admin_picture_delete_favorite_path(favoriteItem.id), {
      method: 'POST',
      credentials: 'include',
      headers: {
        'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').content
      }
    })
    .then(res => res.json())
    .then((favorites) => {
      dispatch(deleteFavoritesSuccess(favorites));
    })
    .catch(() => {
      dispatch(deleteFavoritesFail());
    });
  };
}