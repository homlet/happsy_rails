import Routes from 'routes';
import getHttpParams from 'lib/utils/getHttpParams';
import GalleryTypes from '../constants/GalleryTypes';

export function addPictures(pictures) {
  return {
    type: GalleryTypes.ADD_PICTURES,
    pictures
  };
}

export function removePicture(picture) {
  return {
    type: GalleryTypes.REMOVE_PICTURE,
    picture
  };
}

export function sendFormStart() {
  return {
    type: GalleryTypes.SEND_FORM_START
  };
}

export function sendFormSuccess(gallery) {
  return {
    type: GalleryTypes.SEND_FORM_SUCCESS,
    gallery
  };
}

export function sendFormFail() {
  return {
    type: GalleryTypes.SEND_FORM_FAIL
  };
}

export function sendForm(url, formData, method) {
  return (dispatch) => {
    dispatch(sendFormStart());
    fetch(url, {
      method: method || 'POST',
      body: formData,
      credentials: 'include',
      headers: {
        'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').content
      }
    })
      .then(res => res.json())
      .then((gallery) => {
        dispatch(sendFormSuccess(gallery));
      })
      .catch((err) => {
        dispatch(sendFormFail());
      });
  };
}