import _ from 'lodash';
import async from 'async';
import PictureTypes from '../constants/PictureTypes';
import Routes from 'routes';

const LIMIT_IN_QUEUE = 5;

export function setPictureTitle(picture, value) {
  return {
    type: PictureTypes.SET_TITLE,
    picture,
    value
  };
}


/*------------------- Действия над группой файлов -------------------*/
export function addPictures(pictures) {
  return {
    type: PictureTypes.ADD_PICTURES,
    pictures
  };
}

export function uploadPicturesStart() {
  return {type: PictureTypes.UPLOAD_PICTURES_START};
}

export function uploadPicturesFinish() {
  return {type: PictureTypes.UPLOAD_PICTURES_FINISH};
}

export function uploadPicturesSuccess() {
  return {type: PictureTypes.UPLOAD_PICTURES_SUCCESS};
}

export function uploadPicturesFail() {
  return {type: PictureTypes.UPLOAD_PICTURES_FAIL};
}

/*------------------- Действия над файлом -------------------*/

export function removePicture(picture) {
  return {
    type: PictureTypes.REMOVE_PICTURE,
    picture
  };
}

export function uploadPictureStart(picture) {
  return {
    type: PictureTypes.UPLOAD_PICTURE_START,
    picture
  };
}

export function uploadPictureSuccess(picture) {
  return {
    type: PictureTypes.UPLOAD_PICTURE_SUCCESS,
    picture
  };
}

export function uploadPictureFail(picture) {
  return {
    type: PictureTypes.UPLOAD_PICTURE_FAIL,
    picture
  };
}

export function uploadPictures(pictures) {
  return (dispatch) => {
    dispatch(uploadPicturesStart());
    async.eachLimit(pictures, LIMIT_IN_QUEUE, (picture, next) => {
      dispatch(uploadPicture(picture, next));
    }, () => {
      dispatch(uploadPicturesFinish());
    });
  };
  
}


export function uploadPicture(picture, next) {
  return (dispatch) => {
    dispatch(uploadPictureStart(picture));
    let formData = new FormData();
    formData.append('picture[image]', picture.picture);
    formData.append('picture[title]', picture.title);

    //TODO вынести конфиг с заголовками в utils
    fetch(Routes.api_admin_pictures_path(), {
      method: 'POST',
      body: formData,
      credentials: 'include',
      headers: {
        'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').content
      }
    })
      .then(res => res.json())
      .then(() => {
        dispatch(uploadPictureSuccess(picture));
        if(!_.isUndefined(next)) next();
      })
      .catch(() => {
        dispatch(uploadPictureFail(picture));
        if(!_.isUndefined(next)) next();
      });
  };
}

/*------------------- Модальное окно -------------------*/

export function closeModal() {
  return {
    type: PictureTypes.CLOSE_MODAL
  };
}