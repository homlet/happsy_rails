import React from 'react';
import BigCalendar from 'react-big-calendar';
import DatePicker from 'react-datepicker';
import autobind from 'autobind-decorator';
import getClassName from 'lib/utils/getClassName';

const b_ = getClassName('calendar-index');
const b_btn = getClassName('btn');

export default class CalendarIndex extends React.Component {

  @autobind
  handleChangeDate(pickerDate) {
    this.props.changePickerDate(pickerDate);
  }

  @autobind
  handleSaveEvent() {
    const { pickerDate } = this.props;
    this.props.saveEvent(pickerDate.format());
  }

  @autobind
  handleSelectEvent(event) {
    if(window.confirm(I18n.t('web.admin.calendar.index.delete_event_confirm'))) {
      this.props.deleteEvent(event);
    }
  }

  getEvents() {
    return _.map(this.props.events, (event) => {
      return {
        id: event.id,
        title: 'Назначена свадьба',
        allDay: true,
        start: new Date(event.date),
        end: new Date(event.date)
      };
    });
  }

  render() {
    return <div className={`${b_()} row`}>
      <div className='col-lg-8'>
        <div className='main-box clearfix'>
          <div className='main-box-body clearfix'>
            <h3>
              <span>{I18n.t('web.admin.calendar.index.exist_events')}</span>
            </h3>
            <div className={b_('calendar')}>
              <BigCalendar
                culture='ru'
                events={this.getEvents()}
                onSelectEvent={this.handleSelectEvent}
              />
            </div>

          </div>
        </div>
      </div>
      <div className='col-lg-4'>
        <div className='main-box clearfix'>
          <div className='main-box-body clearfix'>
            <h3>
              <span>{I18n.t('web.admin.calendar.index.add_event')}</span>
            </h3>
            <form action=''>

              <div className="form-group">
                <label>Дата</label>
                <DatePicker
                  locale='ru'
                  selected={this.props.pickerDate}
                  className="form-control"
                  onChange={this.handleChangeDate}
                />

              </div>

              <span
                type='button'
                className={b_btn().mix({'btn-success': true})}
                onClick={this.handleSaveEvent}
              >
                <span className='fa fa-plus'></span> {I18n.t('web.admin.calendar.index.add_event')}
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>;
  }
}