import connectProvider from 'lib/utils/connectProvider';
import store from 'admin/store/store';

import CalendarIndex from './../../containers/Calendar/CalendarIndex';

export default {
  CalendarIndex: connectProvider(CalendarIndex, store)
};