import React, {Component, PropTypes} from 'react';
import _ from 'lodash';
import Modal from '../Shared/Modal';
import autobind from 'autobind-decorator';
import styles from './styles/FavoritesPicture.module.css';

export default class FavoritesPicture extends Component {
  constructor(props) {
    super(props);

  }

  handleToggleFavorite(picture) {
    if(picture.favorite) {
      this.props.deleteFavorites(picture);
    } else {
      this.props.saveFavorites(picture);
    }

  }

  render() {
    return <div className={styles.root}>
      <div className={styles.fileList}>
        {this.renderFavoritePictures()}
      </div>

    </div>;
  }

  renderFavoritePictures() {
    return _.map(this.props.pictures, (picture) => {
      return <div className={styles.fileItem} key={picture.id}>
        <img src={picture.image_path} alt="" className={styles.fileImg}/>
        <div className={styles.fileCheckbox}>
          <input
            type="checkbox"
            className={styles.fileCheckboxInput}
            onChange={this.handleToggleFavorite.bind(this, picture)}
            checked={picture.favorite}
          />
        </div>
      </div>;
    });
  }
}

FavoritesPicture.propTypes = {
  pictures: PropTypes.array,
  saveFavorites: PropTypes.func,
  deleteFavorites: PropTypes.func
};