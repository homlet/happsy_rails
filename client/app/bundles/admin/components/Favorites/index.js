import connectProvider from 'lib/utils/connectProvider';
import store from 'admin/store/store';
import FavoritesPicture from './../../containers/Favorites/FavoritesPicture';

export default {
  FavoritesPicture:  connectProvider(FavoritesPicture, store)
}