import React, { PropTypes } from 'react';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import autobind from 'autobind-decorator';
import FileRow from './Upload/FileRow';
import Input from '../Shared/Input';
import GalleryPresenter from '../../presenters/GalleryPresenter';
import getClassName from 'lib/utils/getClassName';

const b_ = getClassName('gallery-form');
const b_btn = getClassName('btn');

export default class GalleryForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: props.gallery.title
    };
    
    this.handleChange = _.debounce(this.handleChange, 300);
  }

  @autobind
  handleDrop(files) {
    this.props.addPictures(files);
  }

  @autobind
  handleOpenDropzone(e) {
    e.preventDefault();
    this.refs.dropzone.open();
  }

  @autobind
  handleChange(target) {
    this.setState({
      title: target.value
    });
  }

  @autobind
  handleSubmit(e) {
    e.preventDefault();
    let method = this.isExistGallery() ? 'PUT' : 'POST';
    let formData = new FormData(this.refs.form);
    _.each(this.props.pictures, (picture) => {
      formData.append(`gallery[pictures_attributes][${picture.id}][image]`, picture.picture);
    });
    
    this.props.sendForm(this.props.actionUrl, formData, method);
  }

  @autobind
  handleRemoveFile(picture) {
    this.props.removePicture(picture);
  }

  getCategories() {
    return _.map(this.props.categories, (category) => {
      return {
        id: category.id,
        text: category.title
      };
    });
  }

  getCategoryValues() {
    return _.map(this.props.gallery.categories, (category) => {
      return category.id;
    });
  }

  isExistGallery() {
    return !!this.props.gallery.id;
  }

  isExistGalleryPictures() {
    const { pictures, gallery } = this.props;
    return pictures.length !== 0 || (gallery.pictures && gallery.pictures.length !== 0);
  }
  
  render() {
    return <form action={this.props.actionUrl} method='post' className={b_()} onSubmit={this.handleSubmit} ref='form'>
      <Input
        name='gallery[title]'
        label='Название галереи'
        onChange={this.handleChange}
        value={this.state.title}
      />
      <Input
        type='select'
        name='gallery[category_ids][]'
        label='Категории'
        selectData={this.getCategories()}
        selectOptions={{
          placeholder: 'Выберите категорию'
        }}
        value={this.getCategoryValues()}
        multiple={true}
      />

      <Input
        type='file'
        name='gallery[cover]'
        label='Обложка'
      />
      {this.renderCover()}

      <div className={b_('settings')}>
        <div className='button-group'>
          <span type='button' className={b_btn().mix({'btn-success': true})} onClick={this.handleOpenDropzone}>
            <span className='fa fa-plus'></span> Добавить изображения
          </span>
        </div>

      </div>
      {this.renderTable()}
      <button className='btn btn-primary' type='submit'>
        {this.props.submitTitle}
      </button>
      <Dropzone ref='dropzone' onDrop={this.handleDrop} className='dropzone' />
      
    </form>;
  }

  renderFiles() {
    const { pictures } = this.props;
    return _.map(pictures, (picture, i) => {
      return <FileRow key={picture.id}
                      picture={picture}
                      onRemoveFile={this.handleRemoveFile}
                      onSetTitle={this.handleSetTitle}
                      order={i}
      />;
    });
  }

  renderExistFiles() {
    let { existPictures, gallery } = this.props;
    return _.map(existPictures, (picture, i) => {
      return <FileRow key={picture.id}
                      galleryId={gallery.id}
                      picture={picture}
                      onRemoveFile={this.handleRemoveFile}
                      onSetTitle={this.handleSetTitle}
                      order={i}
      />;
    });
  }

  renderTable() {
    if (!this.isExistGalleryPictures()) return;

    return <div className='table-responsive'>
      <table className='table'>
        <thead>
        <tr>
          <th>Изображение</th>
          <th>Название</th>
          <th>Размер</th>
          <th>Подпись</th>
          <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        {this.renderExistFiles()}
        {this.renderFiles()}
        </tbody>
      </table>
    </div>;
  }

  renderCover() {
    if(!GalleryPresenter.cover(this.props.gallery)) return;
    return <div className={b_('cover')}>
      <img src={GalleryPresenter.cover(this.props.gallery)} alt=""/>
    </div>;
  }
}

GalleryForm.propTypes = {
  gallery: PropTypes.object,
  submitTitle: PropTypes.string,
  actionUrl: PropTypes.string,
  addPictures: PropTypes.func,
  sendForm: PropTypes.func,
  removePicture: PropTypes.func,
  pictures: PropTypes.array,
  existPictures: PropTypes.array,
  categories: PropTypes.array,
  attachments: PropTypes.array
};

GalleryForm.defaultProps = {
  gallery: {}
};