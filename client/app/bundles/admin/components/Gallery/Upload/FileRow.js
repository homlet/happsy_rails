import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import autobind from 'autobind-decorator';
import FilePresenter from 'admin/presenters/FilePresenter';
import getClassName from 'lib/utils/getClassName';

const b_ = getClassName('file-row');


// TODO заимплементить вложенные параметры
export default class FileRow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isExistDestroy: false
    };
  }

  handleSetTitle() {
    // TODO implement
  }

  @autobind
  handleRemoveFile(picture, e) {
    e.preventDefault();
    this.props.onRemoveFile(picture);
  }

  @autobind
  handleRemoveExistFile() {
    this.setState({isExistDestroy: true});
  }

  getPreviewSrc() {
    const { picture } = this.props;
    if(this.isExistFiles()) {
      return picture.image_path;
    } else {
      return picture.picture.preview;
    }
  }

  setDestroyValue() {
    if(this.state.isExistDestroy) {
      return 1;
    } else {
      return false;
    }
  }

  setFileSize() {
    const { picture } = this.props;
    if(this.isExistFiles()) {
      return picture.size;
    } else {
      return picture.picture.size;
    }
  }

  isExistFiles() {
    const { picture } = this.props;
    return !picture.picture && picture.id; 
  }

  render() {
    const { picture } = this.props;
    const file = picture.picture;

    return <tr className={b_({'delete-exist-file': this.state.isExistDestroy})}>
      <td width='200'>
        {this.renderHiddenIdField()}
        {this.renderAttachmentInputs()}
        <img src={this.getPreviewSrc()} alt='' width='200'/>
      </td>
      <td >
        <input 
          type="hidden" 
          name={`gallery[pictures_attributes][${this.props.picture.id}][filename]`}
          value={FilePresenter.filename(file || picture)}
        />
        {FilePresenter.filename(file || picture)}
      </td>
      <td>
        <input 
          type="hidden" 
          name={`gallery[pictures_attributes][${this.props.picture.id}][size]`}
          value={this.setFileSize()}
        />
        {FilePresenter.size(file || picture)} мб
      </td>
      <td>
        <textarea
          name={`gallery[pictures_attributes][${this.props.picture.id}][title]`}
          className='form-control'
          onChange={this.handleSetTitle.bind(this, picture)}
          defaultValue={picture.title}
        />
      </td>
      <td>
        {this.renderDeleteButton(picture)}
      </td>
    </tr>;
  }

  renderHiddenIdField() {
    if(this.isExistFiles()) {
      const { galleryId, picture } = this.props;
      const attachment = _.find(picture.attachments, attachment => attachment.gallery_id === galleryId);

      return <div>
        <input
          type="hidden"
          name={`gallery[pictures_attributes][${this.props.picture.id}][id]`}
          value={this.props.picture.id}
        />
        {/*<input
          type="hidden"
          name={`gallery[pictures_attributes][attachments_attributes][${attachment.id}][id]`}
          value={attachment.id}
        />*/}
      </div>;
    }
  }

  renderAttachmentInputs() {
    const { galleryId, picture } = this.props;
    const attachmentId = this.isExistFiles() ? _.find(picture.attachments, attachment => attachment.gallery_id === galleryId).id : this.props.picture.id;

    return <div>
      {/*<input*/}
        {/*type="hidden"*/}
        {/*name={`gallery[pictures_attributes][attachments_attributes][${attachmentId}][order]`}*/}
        {/*value={this.props.order}*/}
      {/*/>*/}
    </div>;
  }

  renderDeleteButton(picture) {
    if(this.isExistFiles()) {
      return <div>
        <input 
          type="hidden" 
          name={`gallery[pictures_attributes][${this.props.picture.id}][_destroy]`}
          value={this.setDestroyValue()}
        />
        <button type='button' className='btn btn-danger' onClick={this.handleRemoveExistFile}>
          <span className='fa fa-trash-o'></span> Удалить
        </button>
      </div>;
    } else {
      return <button type='button' className='btn btn-danger' onClick={this.handleRemoveFile.bind(this, picture)}>
        <span className='fa fa-trash-o'></span> Удалить
      </button>;
    }
  }

}

FileRow.propTypes = {
  key: PropTypes.string,
  image_path: PropTypes.string,
  galleryId: PropTypes.number,
  order: PropTypes.number,
  picture: PropTypes.object,
  onRemoveFile: PropTypes.func
};