import connectProvider from 'lib/utils/connectProvider';
import store from 'admin/store/store';

import GalleryForm from './../../containers/Gallery/GalleryForm';
export default {
  GalleryForm: connectProvider(GalleryForm, store)
};