import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import autobind from 'autobind-decorator';
import Modal from '../Shared/Modal';
import FileRow from './Upload/FileRow';
import { connect } from '../../decorators';
import * as PictureActions from '../../actions/PictureActions';
import Routes from 'routes';

import getClassName from 'lib/utils/getClassName';

const b_ = getClassName('picture-page-new');
const b_notification_error = getClassName('picture-notification-error');
const b_btn = getClassName('btn');

@connect((state) => {
  const { PictureReducer } = state;

  return {
    ...PictureReducer
  };
}, (dispatch) => {
  return bindActionCreators(PictureActions, dispatch);
})

export default class NewPictureFormRoot extends React.Component {

  @autobind
  handleOpenDropzone(e) {
    e.preventDefault();
    this.refs.dropzone.open();
  }

  @autobind
  handleDrop(files) {
    this.props.addPictures(files);
  }

  @autobind
  handleRemoveFile(picture) {
    this.props.removePicture(picture);
  }

  @autobind
  handleSetTitle(picture, value) {
    this.props.setPictureTitle(picture, value);
  }

  @autobind
  handleUpload() {
    this.props.uploadPictures(this.props.pictures);
  }

  @autobind
  handleRedirectToList() {
    window.location.href = Routes.admin_pictures_path();
  }

  @autobind
  handleCloseModal() {
    this.props.closeModal();
  }

  isExistFiles() {
    return !!this.props.countQueuePictures;
  }

  render() {
    return <div className={b_()}>
      <div className={b_('settings')}>
        <div className='button-group'>
          <span type='button' className={b_btn().mix({'btn-success': true})} onClick={this.handleOpenDropzone}>
            <span className='fa fa-plus'></span> Добавить файлы
          </span>
          <span type='button' className={b_btn().mix({'btn-primary': true, hide: !this.isExistFiles()})} onClick={this.handleUpload}>
            <span className='fa fa-download'></span> Загрузить изображения
          </span>
        </div>

      </div>
      <div className='table-responsive'>
        <table className='table'>
          <thead>
          <tr>
            <th>Изображение</th>
            <th>Название</th>
            <th>Размер</th>
            <th>Подпись</th>
            <th>&nbsp;</th>
          </tr>
          </thead>
          <tbody>
          {this.renderFiles()}
          </tbody>
        </table>
      </div>

      <Dropzone ref='dropzone' onDrop={this.handleDrop} className='dropzone' />
      {this.renderModal()}
    </div>;
  }

  renderFiles() {
    const { pictures } = this.props;
    return _.map(pictures, (picture) => {
      return <FileRow key={picture.id}
                      picture={picture}
                      onRemoveFile={this.handleRemoveFile}
                      onSetTitle={this.handleSetTitle}
      />;
    });
  }
  
  renderModal() {
    return <Modal
      isOpen={this.props.isOpenNotificationModal}
      onRequestClose={this.handleCloseModal}
    >
      {this.renderNotification()}
    </Modal>;
  }

  renderNotification() {
    if(this.props.isLoaded && this.props.isFailLoaded) {
      return this.renderFailNotification();
    } else {
      return this.renderSuccesMessage();
    }
  }

  renderFailNotification() {
    return <div className={b_notification_error()}>
      <p className={b_notification_error('title')}>Некоторые файлы не удалось загрузить:</p>
      <div className={b_notification_error('content')}>
        {this.renderFailFilesList()}
      </div>
      <button type="button" className="btn btn-primary" onClick={this.handleCloseModal}>Закрыть</button>
    </div>;
  }

  renderFailFilesList() {
    const failedLoadPicture = _.filter(this.props.pictures, pic => pic.isFailLoaded);

    return _.map(failedLoadPicture, (pic) => {
      const file = pic.picture;
      return <div key={pic.id} className={b_notification_error('item')}>
        <img src={file.preview} className={b_notification_error('item-img')}/>
        <div className={b_notification_error('item-filename')}>
          {file.name}
        </div>
      </div>;
    });
  }

  renderSuccesMessage() {
    return <div>
      <p>Все файлы успешно загруженны</p>
      <button type="button" className="btn btn-primary" onClick={this.handleRedirectToList}>Перейти к списку</button>
    </div>;
  }

}

NewPictureFormRoot.propTypes = {
  isLoaded: PropTypes.bool,
  isFailLoaded: PropTypes.bool,
  isOpenNotificationModal: PropTypes.bool,
  closeModal: PropTypes.func,
  pictures: PropTypes.array,
  countQueuePictures: PropTypes.number,
  addPictures: PropTypes.func,
  removePicture: PropTypes.func,
  setPictureTitle: PropTypes.func,
  uploadPictures: PropTypes.func
};