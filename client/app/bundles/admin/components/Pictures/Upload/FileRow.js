import React from 'react';
import FilePresenter from 'admin/presenters/FilePresenter';
import getClassName from 'lib/utils/getClassName';

const b_ = getClassName('file-row');

export default class FileRow extends React.Component {
  handleRemoveFile(picture, e) {
    e.preventDefault();
    this.props.onRemoveFile(picture);
  }

  handleSetTitle(picture, e) {
    this.props.onSetTitle(picture, e.target.value);
  }

  render() {
    const { picture } = this.props;
    const file = picture.picture;
    
    return <tr className={b_()}>
      <td width='200'><img src={file.preview} alt='' width='200'/></td>
      <td >
        {file.name}
        {this.renderLoader()}
        {this.renderLoadStatus()}
      </td>
      <td>{FilePresenter.size(file)} мб</td>
      <td>
        <textarea
          name='picture[title]'
          className='form-control'
          onChange={this.handleSetTitle.bind(this, picture)}
        />
      </td>
      <td>
        <button type='button' className='btn btn-danger' onClick={this.handleRemoveFile.bind(this, picture)}>
          <span className='fa fa-trash-o'></span> Удалить
        </button>
      </td>
    </tr>;
  }

  renderLoader() {
    const { picture } = this.props;
    if (picture.loadedProcess) {
      return <i className={b_('loader').mix('fa fa-spinner fa-spin')}></i>;
    }
  }

  renderLoadStatus() {
    const { picture } = this.props;
    if (picture.isLoaded) {
      return <div><span className='label label-success'>Загруженно</span></div>;
    } else if (picture.isFailLoaded) {
      return <div><span className='label label-danger'>Не загруженно</span></div>;
    }
  }
}