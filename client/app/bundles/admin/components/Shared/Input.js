import React, { Component, PropTypes } from 'react';
import autobind from 'autobind-decorator';
import _ from 'lodash';
import Select2 from 'react-select2-wrapper';

export default class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({value: nextProps.value});
  }

  @autobind
  handleChange(e) {
    if(this.props.onChange) {
      this.props.onChange(e.target);
    }
    this.setState({value: e.target.value});
  }
  
  getIdFromName() {
    return this.props.name.replace(/[\[{(]/g, '_').replace(/[\]})]/g, '');
  }

  render() {
    const id = _.uniqueId(`${this.getIdFromName()}_`);

    return <div className='form-group'>
      <label htmlFor={id}>{this.props.label}</label>
      {this.renderTypeInput(id)}
    </div>;
  }

  renderTypeInput(id) {
    const { type, selectData, selectOptions, multiple, value, name, placeholder } = this.props;

    switch (type) {
      case 'select':
        return <Select2
          defaultValue={value}
          multiple={multiple}
          className='form-control'
          name={name}
          data={selectData}
          options={selectOptions}
        />;
      default:
        return <input
          type={type}
          className='form-control'
          name={name}
          id={id}
          placeholder={placeholder}
          value={this.state.value}
          defaultValue={value}
          onChange={this.handleChange}
        />;
    }
  }
}
Input.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.any,
  selectData: PropTypes.any,
  selectOptions: PropTypes.object,
  multiple: PropTypes.bool,
  onChange: PropTypes.func

};