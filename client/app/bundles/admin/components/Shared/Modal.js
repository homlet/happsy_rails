import React, { PropTypes } from 'react';
import ReactModal from 'react-modal';
import SettingsService from 'lib/utils/settingsService';
import getClassName from 'lib/utils/getClassName';
// import {svgIcon} from 'lib/utils/images';

const b_ = getClassName('modal');
const modalResetDefaultStyle = SettingsService.getSetting('modal.modalResetDefaultStyle');

export default class Modal extends React.Component {
  constructor(props) {
    super(props);

    ReactModal.setAppElement('body');
  }

  componentDidUpdate() {
    this._setBobyPadding();
  }

  render() {
    return <ReactModal
      isOpen={this.props.isOpen}
      onRequestClose={this.props.onRequestClose}
      closeTimeoutMS={this.props.closeTimeoutMS}
      className=''
      style={modalResetDefaultStyle}
    >

      <div className='modal-content'>
        {this.renderHeadline()}
        <div className='modal-body'>
          {this.props.children}
        </div>
        {this.renderFooter()}
      </div>
      {this.renderCloseButton()}
    </ReactModal>;
  }

  renderHeadline() {
    if (this.props.title) {
      return <header className='modal-header'>
        <h4 className='modal-title'>
          {this.props.title}
        </h4>
      </header>;
    }
  }

  renderFooter() {
    if (this.props.footer) {
      return <footer className='modal-footer'>
        {this.props.footer}
      </footer>;
    }
  }

  renderCloseButton() {
    if (!this.props.disableCloseButton) {
      return <span
        className={b_('close', this.props.closeButtonModificators).mix(this.props.closeButtonClassName) }
        onClick={this.props.onRequestClose}>
        {this.renderIconCloseButton()}
      </span>;
    }
  }

  renderIconCloseButton() {
    return this.props.iconCloseButton ?
      this.props.iconCloseButton :
      this.renderDefaultCloseButton();
  }

  renderDefaultCloseButton() {
    return null;//svgIcon('common/modal_close')
  }

  _setBobyPadding() {
    var bodyNode = document.body;
    var fullWindowWidth = window.innerWidth;
    var isBodyOverflowing = bodyNode.clientWidth < fullWindowWidth;
    var scrollbarWidth = this._getScrollbarWidth();

    if (this.props.isOpen) {
      bodyNode.classList.add(b_('is-open'));
      bodyNode.style.paddingRight = isBodyOverflowing ? `${scrollbarWidth}px` : '';
    } else {
      bodyNode.classList.remove(b_('is-open'));
      bodyNode.style.paddingRight = '';
    }
  }

  _getScrollbarWidth() {
    var bodyNode = document.body;
    var scrollDiv = document.createElement('div');
    scrollDiv.className = b_('scrollbar-measure');
    bodyNode.appendChild(scrollDiv);
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    bodyNode.removeChild(scrollDiv);
    return scrollbarWidth;
  }
}

Modal.propTypes = {
  children: PropTypes.any,
  isOpen: PropTypes.bool,
  disableCloseButton: PropTypes.bool,
  title: PropTypes.string,
  closeButtonClassName: PropTypes.string,
  classNameModificators: PropTypes.object,
  closeButtonModificators: PropTypes.object,
  onRequestClose: PropTypes.func,
  closeTimeoutMS: PropTypes.func,
  footer: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]),
  iconCloseButton: PropTypes.any
  
};
