import keyMirror from 'lib/utils/keyMirror';

export default keyMirror({
  CHANGE_PICKER_DATE: null,

  SAVE_EVENT_START: null,
  SAVE_EVENT_SUCCESS: null,
  SAVE_EVENT_FAIL: null,

  DELETE_EVENT_START: null,
  DELETE_EVENT_SUCCESS: null,
  DELETE_EVENT_FAIL: null
}, 'CALENDAR_');