import keyMirror from 'lib/utils/keyMirror';

export default keyMirror({
  SAVE_FAVORITE_START: null,
  SAVE_FAVORITE_SUCCESS: null,
  SAVE_FAVORITE_FAIL: null,

  DELETE_FAVORITE_START: null,
  DELETE_FAVORITE_SUCCESS: null,
  DELETE_FAVORITE_FAIL: null,
}, 'FAVORITE_');