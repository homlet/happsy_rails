import keyMirror from 'lib/utils/keyMirror';

export default keyMirror({
  ADD_PICTURES: null,
  REMOVE_PICTURE: null,
  SET_TITLE: null,

  SEND_FORM_START: null,
  SEND_FORM_SUCCESS: null,
  SEND_FORM_FAIL: null
}, 'GALLERY_');