import keyMirror from 'lib/utils/keyMirror';

export default keyMirror({
  UPLOAD_PICTURES_START: null,
  UPLOAD_PICTURES_FINISH: null,
  UPLOAD_PICTURES_SUCCESS: null,
  UPLOAD_PICTURES_FAIL: null,

  UPLOAD_PICTURE_START: null,
  UPLOAD_PICTURE_SUCCESS: null,
  UPLOAD_PICTURE_FAIL: null,

  ADD_PICTURES: null,
  REMOVE_PICTURE: null,
  SET_TITLE: null,
  
  CLOSE_MODAL: null
}, 'PICTURE_');