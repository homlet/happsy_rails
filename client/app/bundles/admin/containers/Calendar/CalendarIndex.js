import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as CalendarActions from '../../actions/CalendarActions';
import CalendarIndex from '../../components/Calendar/CalendarIndex';

function mapStateToProps(state) {
  return {...state.CalendarReducer};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(CalendarActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarIndex);
