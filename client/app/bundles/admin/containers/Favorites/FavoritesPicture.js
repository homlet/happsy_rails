import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as FavoriteActions from '../../actions/FavoriteActions';
import FavoritesPicture from '../../components/Favorites/FavoritesPicture';

function mapStateToProps(state) {
  return {...state.FavoriteReducer};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(FavoriteActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesPicture);
