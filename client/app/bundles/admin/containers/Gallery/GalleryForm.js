import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as GalleryActions from '../../actions/GalleryActions';
import GalleryForm from '../../components/Gallery/GalleryForm';

function mapStateToProps(state) {
  return {...state.GalleryReducer};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(GalleryActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GalleryForm);
