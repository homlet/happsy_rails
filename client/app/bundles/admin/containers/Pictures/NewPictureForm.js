import React from 'react';
import { Provider } from 'react-redux';
import store from 'admin/store/store';

import NewPictureFormRoot from 'admin/components/Pictures/NewPictureFormRoot';

export default class NewPictureForm extends React.Component{
  render() {
    return <Provider store={store}>
      <NewPictureFormRoot />
    </Provider>;
  }
}