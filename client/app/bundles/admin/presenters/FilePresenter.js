export default {
  filename(file) {
    return file.name || file.filename;
  },

  size(file) {
    return Math.round(file.size / 10000) / 100;
  }
};