import _ from 'lodash';
import moment from 'moment';

import CalendarTypes from '../constants/CalendarTypes';
import createReducer from 'lib/utils/createReducer';

const initialState = {
  pickerDate: moment(),
  events: []
};

function changePickerDate(state, action) {
  const { pickerDate } = action;

  return {...state, pickerDate};
}

function saveEventSuccess(state, action) {
  return {...state,
    events: [...state.events, action.event]
  };
}

function deleteEventSuccess(state, action) {
  const { event } = action;
  const events = _.filter(state.events, (e) => e.id !== event.id);
  return {...state, events};
}

const actionHandlers = {
  [CalendarTypes.CHANGE_PICKER_DATE]: changePickerDate,
  [CalendarTypes.SAVE_EVENT_SUCCESS]: saveEventSuccess,
  [CalendarTypes.DELETE_EVENT_SUCCESS]: deleteEventSuccess
};

export default createReducer(initialState, actionHandlers);