import _ from 'lodash';
import FavoriteTypes from '../constants/FavoriteTypes';
import createReducer from 'lib/utils/createReducer';

const initialState = {
  pictures: []
};

function saveFavoriteStart(state, action) {
  const { picture } = action;
  return {...state};
}
function saveFavoriteSuccess(state, action) {
  const { picture } = action;
  picture.favorite = true;
  const statePictures = _.map(state.pictures, pic => {
    if(pic.id === picture.id) {
      return picture;
    } else {
      return pic;
    }
  });

  return {...state,
    pictures: statePictures
  };
}
function saveFavoriteFail(state, action) {
  const { picture } = action;
  return {...state};
}

function deleteFavoriteStart(state, action) {
  const { picture } = action;
  return {...state};
}
function deleteFavoriteSuccess(state, action) {
  const { picture } = action;
  picture.favorite = false;
  const statePictures = _.map(state.pictures, pic => {
    if(pic.id === picture.id) {
      return picture;
    } else {
      return pic;
    }
  });

  return {...state,
    pictures: statePictures
  };
}

function deleteFavoriteFail(state, action) {
  return {...state};
}


const actionHandlers = {
  [FavoriteTypes.SAVE_FAVORITE_START]: saveFavoriteStart,
  [FavoriteTypes.SAVE_FAVORITE_SUCCESS]: saveFavoriteSuccess,
  [FavoriteTypes.SAVE_FAVORITE_FAIL]: saveFavoriteFail,

  [FavoriteTypes.DELETE_FAVORITE_START]: deleteFavoriteStart,
  [FavoriteTypes.DELETE_FAVORITE_SUCCESS]: deleteFavoriteSuccess,
  [FavoriteTypes.DELETE_FAVORITE_FAIL]: deleteFavoriteFail
};

export default createReducer(initialState, actionHandlers);