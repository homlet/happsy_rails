import _ from 'lodash';
import Routes from 'routes';
import GalleryTypes from '../constants/GalleryTypes';
import createReducer from 'lib/utils/createReducer';

const initialState = {
  pictures: []
};

function addPictures(state, action) {
  const pictures = _.map(action.pictures, (picture) => {
    return {
      id: new Date().getTime() + _.uniqueId(),
      attachmentId: new Date().getTime() + _.uniqueId(),
      title: '',
      picture: picture
    };
  });

  return {...state,
    pictures: [...state.pictures, ...pictures]
  };
}

function removePicture(state, action) {
  const { picture } = action;

  return {...state,
    pictures: _.filter(state.pictures, pic => pic.id !== picture.id)
  };
}

function sendFormSuccess(state, action) {
  window.location.pathname =  Routes.admin_galleries_path();
  return {...state};
}

function sendFormFail(state) {
  return {...state};
}

const actionHandlers = {
  [GalleryTypes.ADD_PICTURES]: addPictures,
  [GalleryTypes.REMOVE_PICTURE]: removePicture,
  [GalleryTypes.SEND_FORM_SUCCESS]: sendFormSuccess, 
  [GalleryTypes.SEND_FORM_FAIL]: sendFormFail 
};

export default createReducer(initialState, actionHandlers);