import _ from 'lodash';
import PictureTypes from '../constants/PictureTypes';
import createReducer from 'lib/utils/createReducer';

const initialState = {
  pictures: [],
  isLoaded: false,
  isFailLoaded: false,
  loadedProcess: false,
  countQueuePictures: 0,
  isOpenNotificationModal: false
};

function addPictures(state, action) {
  const pictures = _.map(action.pictures, (picture) => {
    return {
      id: _.uniqueId('picture_'),
      isLoaded: false,
      isFailLoaded: false,
      loadedProcess: false,
      title: '',
      picture: picture
    };
  });

  return {...state,
    pictures: [...state.pictures, ...pictures],
    countQueuePictures: state.countQueuePictures + pictures.length
  };
}

function uploadPicturesStart(state) {
  return {...state, loadedProcess: true};
}

function uploadPicturesFinish(state) {
  return {...state, loadedProcess: false, isLoaded: true, isOpenNotificationModal: true};
}

//---------------------

function removePicture(state, action) {
  const { picture } = action;

  return {...state,
    pictures: _.filter(state.pictures, pic => pic.id !== picture.id),
    countQueuePictures: state.countQueuePictures - 1
  };
}

function uploadPictureStart(state, action) {
  const { picture } = action;
  const newPicturesState = _.map(state.pictures, (pic) => {
    return pic.id === picture.id ?
      {...pic, loadedProcess: true} :
      pic;
  });

  return {...state, pictures: newPicturesState};
}

function uploadPictureSuccess(state, action) {
  const { picture } = action;
  const newPicturesState = _.map(state.pictures, (pic) => {
    if(pic.id === picture.id) {
      return {...pic,
        isLoaded: true,
        loadedProcess: false
      };
    } else {
      return pic;
    }
  });

  return {...state, pictures: newPicturesState};
}

function uploadPictureFail(state, action) {
  const { picture } = action;
  const newPicturesState = _.map(state.pictures, (pic) => {
    if(pic.id === picture.id) {
      return {...pic,
        isFailLoaded: true,
        loadedProcess: false
      };
    } else {
      return pic;
    }
  });

  return {...state, isFailLoaded: true, pictures: newPicturesState};
}

function setPictureTitle(state, action) {
  const { picture, value } = action;
  const newPicturesState = _.map(state.pictures, (pic) => {
    if(pic.id === picture.id) {
      return {...pic,
        title: value
      };
    } else {
      return pic;
    }
  });
  
  return {...state, pictures: newPicturesState};
}


function closeModal(state) {
  return {...state, isOpenNotificationModal: false};
}

const actionHandlers = {
  [PictureTypes.ADD_PICTURES]: addPictures,
  [PictureTypes.REMOVE_PICTURE]: removePicture,

  [PictureTypes.UPLOAD_PICTURES_START]: uploadPicturesStart,
  [PictureTypes.UPLOAD_PICTURES_FINISH]: uploadPicturesFinish,

  [PictureTypes.UPLOAD_PICTURE_START]: uploadPictureStart,
  [PictureTypes.UPLOAD_PICTURE_SUCCESS]: uploadPictureSuccess,
  [PictureTypes.UPLOAD_PICTURE_FAIL]: uploadPictureFail,

  [PictureTypes.SET_TITLE]: setPictureTitle,
  
  [PictureTypes.CLOSE_MODAL]: closeModal
};

export default createReducer(initialState, actionHandlers);