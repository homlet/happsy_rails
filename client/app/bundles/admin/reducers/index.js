import _ from 'lodash';
import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

import PictureReducer from './PictureReducer';
import CalendarReducer from './CalendarReducer';
import GalleryReducer from './GalleryReducer';
import FavoriteReducer from './FavoriteReducer';

const rootReducer = combineReducers(gonState({
  form,
  PictureReducer,
  CalendarReducer,
  GalleryReducer,
  FavoriteReducer
}));

function gonState(reducers) {
  return _.fromPairs(_.map(reducers, (reducer, key) => {
    let value = (state, action) => {
      if (_.isUndefined(state) && window.gon)
        state = { ...reducer(), ...window.gon[key] };
      return reducer(state, action);
    };
    return [key, value];
  }));
}

export default rootReducer;
