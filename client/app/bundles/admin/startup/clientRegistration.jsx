import ReactOnRails from 'react-on-rails';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import BigCalendarLocalizer from 'react-big-calendar/lib/localizers/moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-select2-wrapper/css/select2.css';
import moment from 'moment';

BigCalendarLocalizer(moment);

//import Pictures from '../components/Pictures';
import NewPictureForm from '../containers/Pictures/NewPictureForm';
import Calendar from '../components/Calendar';
import Gallery from '../components/Gallery';
import Favorites from '../components/Favorites';

console.log(Calendar);

ReactOnRails.register({
  NewPictureForm,
  CalendarIndex: Calendar.CalendarIndex,
  GalleryForm: Gallery.GalleryForm,
  'Favorites.FavoritesPicture': Favorites.FavoritesPicture
});