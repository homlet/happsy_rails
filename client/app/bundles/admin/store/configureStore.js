import { createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import combineActionsMiddleware from 'redux-combine-actions';
import rootReducer from '../reducers';

const createStoreWithMiddleware = applyMiddleware(
  thunk,
  combineActionsMiddleware
)(createStore);

export default function configureStore(initialState) {
  return createStoreWithMiddleware(rootReducer, initialState);
}
