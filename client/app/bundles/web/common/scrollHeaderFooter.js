import $ from 'jquery';

const OFFSET_COUNT = 60;

export default class ScrollHeaderFooter {
  constructor() {
    this.isShowPanels = true;
    this.$nodeCollection = $('.js-footer, .js-header');
    this.$window = $(window);
    this.lastScrollTop = 0;

    $(window).on('scroll', this.handleScroll.bind(this));
  }

  handleScroll() {
    const scrollTop = this.$window.scrollTop();
    const isScrollDown = scrollTop > this.lastScrollTop + OFFSET_COUNT;
    const isScrollUp = scrollTop < this.lastScrollTop;

    // TODO какой то треш с условиями
    if ((isScrollDown) && this.isShowPanels) {
      this.$nodeCollection.addClass('hide-panels');
      this.isShowPanels = false;
      this.lastScrollTop = scrollTop;
    } else if(isScrollUp && !this.isShowPanels) {
      this.$nodeCollection.removeClass('hide-panels');
      this.isShowPanels = true;
    } else if (scrollTop > this.lastScrollTop && !this.isShowPanels) {
      this.lastScrollTop = scrollTop;
    }

    if(isScrollUp) {
      this.lastScrollTop = scrollTop;
    }
  }
}