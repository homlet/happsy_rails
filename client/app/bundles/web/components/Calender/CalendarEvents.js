import React, { Component, PropTypes } from 'react';
import $ from 'jquery';
import _ from 'lodash';
import styles from './styles/calendar.module.css';

export default class CalendarEvents extends Component {

  componentDidMount() {
    const events = this.getEvents();
    $(this.refs.calendar).calendar({
      style:'background',
      language: 'ru',
      dataSource: [...events]
    });
  }

  getEvents() {
    return _.map(this.props.events, (event) => {
      const date = event.date.split('-');
      return {
        startDate: new Date(date[0], date[1] - 1, date[2]),
        endDate: new Date(date[0], date[1] - 1, date[2]),
        color: 'rgba(0,0,0,.2)'
      };
    });
  }

  render() {
    return <div>
      <div ref="calendar" className={styles.yearCalender}></div>
    </div>;
  }
}

CalendarEvents.propTypes = {
  events: PropTypes.array
};