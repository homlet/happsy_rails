import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import LazyLoad from 'react-lazyload';
import styles from './styles/gallery.module.css';

export default class Gallery extends Component {
  render() {
    return <div className={styles.root}>
      <div className="l-center">{this.renderPictures()}</div>
    </div>;
  }

  renderPlaceholder() {
    return <div className={styles.itemPlaceholder}>
      <i className="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>;
  }

  renderPictures() {
    return _.map(this.props.pictures, (picture) => {
      return <LazyLoad
        key={`picture_${picture.id}`}
        height={200}
        once={true}
        debounce={500}
        placeholder={this.renderPlaceholder()}
      >
        <div className={styles.item}>
          <img src={picture.image_path} alt="" className={styles.itemImg}/>
        </div>
      </LazyLoad>;
    });

  }
}

Gallery.propTypes = {
  pictures: PropTypes.array
};