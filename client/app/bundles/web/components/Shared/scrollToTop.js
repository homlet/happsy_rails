import React from 'react';
import ScrollTop from 'react-scroll-up';
import styles from './styles/scrollToTop.module.css';


const OFFSET_TO_SHOW = 700;

const style = {
  bottom: 80,
  left: 20,
  position: 'fixed',
  cursor: 'pointer',
  transitionDuration: '0.2s',
  transitionTimingFunction: 'ease',
  transitionDelay: '0s',
  zIndex: 100
};

export default class ScrollToTop extends React.Component {
  render() {
    return <ScrollTop showUnder={OFFSET_TO_SHOW} duration={300} style={style}>
      <span className={styles.root}>
        <i className={`fa fa-arrow-up ${styles.icon}`}></i>
      </span>
    </ScrollTop>;
  }
}
