import React, { Component, PropTypes } from 'react';
import styles from './styles/favorites.module.css';

export default class FavoriteGallery extends Component {
  componentDidMount() {
    $(this.refs.gallery).swiper({
      effect: 'fade',
      autoplay: 5000,
      speed: 600,
      pagination: this.refs.pagination,
      paginationClickable: true
    });
  }

  render() {
    return <div className={styles.root}>
      <div className={`swiper-container ${styles.galleryContainer}`} ref="gallery">
        <div className={`swiper-wrapper ${styles.galleryWrapper}`}>
          {this.renderPictures()}
        </div>
        <div className={styles.pagination}>
          <div className="swiper-pagination" ref="pagination"></div>
        </div>
      </div>
    </div>;
  }

  renderPictures() {
    return this.props.pictures.map((picture) => {
      return <div
        className={`swiper-slide ${styles.galleryWrapper}`}
        style={{backgroundImage: `url('${picture.image_path}')`}}
        key={`favoite_${picture.id}`}
      >
      </div>;
    });
  }
}

FavoriteGallery.propTypes = {
  pictures: PropTypes.array
};