import 'swiper/dist/js/swiper.jquery.min';
import 'swiper/dist/css/swiper.min.css';
import 'bootstrap-year-calendar/css/bootstrap-year-calendar.min.css';
import ReactOnRails from 'react-on-rails';
import 'bootstrap-year-calendar';
import 'bootstrap-year-calendar/js/languages/bootstrap-year-calendar.ru';
import '../staticScript';

import Galleries from '../components/Galleries';
import Welcome from '../components/Welcome';
import CalendarEvents from '../components/Calender/CalendarEvents';
import ScrollToTop from '../components/Shared/scrollToTop';


ReactOnRails.register({
  Gallery: Galleries.Gallery,
  WelcomeFavoriteGallery: Welcome.FavoriteGallery,
  CalendarEvents,
  ScrollToTop
});