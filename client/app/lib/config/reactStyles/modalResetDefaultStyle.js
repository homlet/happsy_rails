export default {
  overlay: {
    backgroundColor: null
  },
  content: {
    position: null,
    top: null,
    left: null,
    right: null,
    bottom: null,
    border: null,
    background: null,
    overflow: null,
    WebkitOverflowScrolling: null,
    borderRadius: null,
    outline: null,
    padding: null
  }
};