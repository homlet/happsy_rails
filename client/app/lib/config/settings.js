import modalResetDefaultStyle from './reactStyles/modalResetDefaultStyle';
const defaultCarouselSettings = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  adaptiveHeight: true,
  dots: false
};

const settings = {
  sliders: {
    cardCarousel: {
      ...defaultCarouselSettings,
      infinite: true,
      className: 'slick-slider_card-carousel'
    },
    photoGallery: {
      ...defaultCarouselSettings,
      infinite: false,
      className: 'slick-slider_photo-gallery'
    },
    placeGallery: {
      ...defaultCarouselSettings,
      className: 'slick-slider_place-gallery'
    },
    routeCarousel: {
      ...defaultCarouselSettings,
      infinite: false,
      className: 'slick-slider_route-carousel'
    },
    placesCardCarousel: {
      ...defaultCarouselSettings,
      infinite: false,
      className: 'slick-slider_place-card-carousel'
    },
    itemsCarousel: {
      ...defaultCarouselSettings,
      infinite: false,
      className: 'slick-slider_item-carousel'
    },
    itemsCardCarousel: {
      ...defaultCarouselSettings,
      className: 'slick-slider_item-card-carousel'
    },
    thumbCarousel: {
      ...defaultCarouselSettings,
      infinite: false,
      className: 'slick-slider_thumb-carousel'
    },
    articlesCarousel: {
      ...defaultCarouselSettings,
      infinite: true,
      className: 'slick-slider_articles-carousel'
    },
    routeItemsCarousel: {
      ...defaultCarouselSettings,
      infinite: false,
      className: 'slick-slider_route-items-carousel'
    }
  },
  modal: {
    modalResetDefaultStyle: modalResetDefaultStyle
  },
  masonry: {
    review: {
      isFitWidth: true,
      columnWidth: 380,
      gutter: 30
    },
    news: {
      isFitWidth: true,
      columnWidth: 270,
      gutter: 30
    }
  },
  configurator: {
    viewMode: {
      collapse: 'collapse',
      expand: 'expand',
      tiny: 'tiny'
    },
    typeForm: {
      place: 'place',
      rest: 'rest'
    }
  }
};

export default settings
