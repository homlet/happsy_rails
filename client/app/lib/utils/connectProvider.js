import React from 'react';
import {Provider} from 'react-redux';

export default function connectProvider(Component, store) {
  const ProviderWrapper = React.createClass({
    render() {
      return <Provider store={store}>
        <Component {...this.props}/>
      </Provider>;
    }
  });
  return ProviderWrapper;
}
