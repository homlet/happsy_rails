import _ from 'lodash';

export default function createReducer(defaultState, actionHandlers){
  return (state = defaultState, action = {}) => {
    const reduceFn = actionHandlers[action.type];
    return _.isFunction(reduceFn) ? reduceFn(state, action) : state;
  };
}