export default function (model) {
  return function(name) {
    return `${model}[${name}]`;
  };
}