'use strict';

import getClassName from 'bem-cn';

getClassName.setup({
  el: '__',
  mod: '_'
});

export default getClassName;
