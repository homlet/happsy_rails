export default function getHttpHeaders(options = {}) {
  return Object.assign(options, {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRF-Token': getAuthenticityToken(),
    'X-Requested-With': 'XMLHttpRequest'
  });
}

function getAuthenticityToken() {
  var header = document.querySelector('meta[name="csrf-token"]').content;
  return header;
}
