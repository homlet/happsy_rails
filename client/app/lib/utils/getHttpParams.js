import getHttpHeaders from 'lib/utils/getHttpHeaders';

export default function getHttpParams(method, body, headers = {}) {
  return { method, credentials: 'include', headers: getHttpHeaders(headers), body };
}
