import settings from 'lib/config/settings';
import _ from 'lodash';

class SettingsService{
  static getSetting(settingsPath){
    var splitSettings = settingsPath.split('.');
    var settingValue = _.extend({}, settings);

    for(let i = 0; i < splitSettings.length; i++){
      settingValue = settingValue[splitSettings[i]];
      if(_.isUndefined(settingValue)){
        break;
      }
    }

    return settingValue;
  }
}

export default SettingsService;