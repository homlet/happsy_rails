// Common client-side webpack configuration used by webpack.hot.config and webpack.rails.config.

const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');

const devBuild = process.env.NODE_ENV !== 'production';
const nodeEnv = devBuild ? 'development' : 'production';

module.exports = {

  // the project dir
  context: __dirname,
  entry: {

    // See use of 'vendor' in the CommonsChunkPlugin inclusion below.
    vendor: [
      'babel-polyfill',
      'jquery'
    ],

    // This will contain the app entry points defined by webpack.hot.config and
    // webpack.rails.config
    web: [
      './app/bundles/web/startup/clientRegistration',
      './assets/stylesheets/web/index.scss'
    ],

    admin: [
      './app/bundles/admin/startup/clientRegistration',
      './assets/stylesheets/admin/index.scss'
    ]
  },
  externals: {
    'assetPath': 'asset_path',
    'routes': 'Routes',
    'gon': 'gon'
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      lib: path.join(process.cwd(), 'app', 'lib'),
      react: path.resolve('./node_modules/react'),
      'react-dom': path.resolve('./node_modules/react-dom')
    },
    modulesDirectories: [
      'node_modules',
      'client/app',
      'client/app/bundles'
    ]
  },
  resolveLoader: {
    root: path.join(__dirname, 'node_modules')
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(nodeEnv)
      }
    }),

    new webpack.ProvidePlugin({
      'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
    }),

    // https://webpack.github.io/docs/list-of-plugins.html#2-explicit-vendor-chunk
    new webpack.optimize.CommonsChunkPlugin({

      // This name 'vendor' ties into the entry definition
      name: 'vendor',

      // We don't want the default vendor.js name
      filename: 'vendor-bundle.js',

      // Passing Infinity just creates the commons chunk, but moves no modules into it.
      // In other words, we only put what's in the vendor entry definition in vendor-bundle.js
      minChunks: Infinity
    })
  ],
  module: {
    loaders: [
      { test: /\.(woff2?)$/, loader: 'url?limit=10000' },
      { test: /\.(ttf|eot)$/, loader: 'url?limit=10000' },
      { test: /\.(jpe?g|png|gif|ico)$/, loader: 'url?limit=10000' },

      { test: require.resolve('jquery'), loader: 'expose?jQuery' },
      { test: require.resolve('jquery'), loader: 'expose?$' },

      { test: /\.json$/, loaders: ['json-loader'] },
      { test: /\.gif$/, loader: 'url-loader' },
      { test: /\.svg$/, loader: 'svg-inline?removeSVGTagAttrs=false&removeTags=false' },
      { test: /\.swf$/, loader: 'file-loader' }
    ]
  },

  // Place here all postCSS plugins here, so postcss-loader will apply them
  postcss: [autoprefixer],
  sassResources: ['./assets/stylesheets/app-variables.scss']
};
