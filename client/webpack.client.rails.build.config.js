const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = require('./webpack.client.base.config');

const devBuild = process.env.NODE_ENV !== 'production';

config.output = {
  filename: '[name]-bundle.js',
  path: '../app/assets/webpack'
};

// You can add entry points specific to rails here
// The es5-shim/sham is for capybara testing
config.entry.vendor.unshift(
  'es5-shim/es5-shim',
  'es5-shim/es5-sham'
);

// jquery-ujs MUST GO AFTER jquery, so must use 'push'
config.entry.vendor.push('jquery-ujs');

// See webpack.common.config for adding modules common to both the webpack dev server and rails
config.module.loaders.push(
  {
    test: /\.jsx?$/,
    loader: 'babel-loader',
    exclude: /node_modules/
  },
  {
    test: /\.css$/,
    exclude: [
      path.resolve(__dirname, 'app', 'bundles')
    ],
    loader: ExtractTextPlugin.extract(
      'style',
      'css!postcss'
    )
  },
  {
    test: /\.module.css$/,
    include: [
      path.resolve(__dirname, 'app', 'bundles')
    ],
    loader: ExtractTextPlugin.extract(
      'style',
      'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]' +
      '!postcss'
    )
  },
  {
    test: /\.scss$/,
    loader: ExtractTextPlugin.extract(
      'style',
      'css!postcss!sass!sass-resources'
    )
  },
  {
    test: require.resolve('react'),
    loader: 'imports?shim=es5-shim/es5-shim&sham=es5-shim/es5-sham'
  },
  {
    test: require.resolve('jquery-ujs'),
    loader: 'imports?jQuery=jquery'
  }
);

config.plugins.push(
  new webpack.DefinePlugin({ 'process.env': { 'NODE_ENV': JSON.stringify('production') } }),
  new ExtractTextPlugin('[name]-bundle.css', { allChunks: true }),
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.OccurenceOrderPlugin()
);



if (devBuild) {
  console.log('Webpack dev build for Rails'); // eslint-disable-line no-console
  module.devtool = 'eval-source-map';
} else {
  console.log('Webpack production build for Rails'); // eslint-disable-line no-console
}

module.exports = config;