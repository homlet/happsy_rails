// Run with Rails server like this:
// rails s
// cd client && babel-node server-rails-hot.js
// Note that Foreman (Procfile.dev) has also been configured to take care of this.

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = require('./webpack.client.base.config');

const hotRailsPort = process.env.HOT_RAILS_PORT || 9090;
const hotRailsHost = process.env.HOT_RAILS_HOST || '0.0.0.0';

config.entry.web.push(
  `webpack-dev-server/client?http://${hotRailsHost}:${hotRailsPort}`,
  'webpack/hot/only-dev-server'
);

config.entry.vendor.push(
  'es5-shim/es5-shim',
  'es5-shim/es5-sham',
  'jquery-ujs'
);

config.output = {
  filename: '[name]-bundle.js',
  path: path.join(__dirname, 'public'),
  publicPath: `http://${hotRailsHost}:${hotRailsPort}/`
};

config.module.loaders.push(
  {
    test: /\.jsx?$/,
    loader: 'babel',
    exclude: /node_modules/,
    query: {
      plugins: [
        [
          'react-transform',
          {
            transforms: [
              {
                transform: 'react-transform-hmr',
                imports: ['react'],
                locals: ['module']
              }
            ]
          }
        ]
      ]
    }
  },
  {
    test: /\.css$/,
    exclude: [
      path.resolve(__dirname, 'app', 'bundles')
    ],
    loader: ExtractTextPlugin.extract(
      'style',
      'css!postcss'
    )
  },
  {
    test: /\.module.css$/,
    include: [
      path.resolve(__dirname, 'app', 'bundles')
    ],
    loader: ExtractTextPlugin.extract(
      'style',
      'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]' +
      '!postcss'
    )
  },
  {
    test: /\.scss$/,
    loader: ExtractTextPlugin.extract(
      'style',
      'css!postcss!sass!sass-resources'
    )
  },
  {
    test: require.resolve('jquery-ujs'),
    loader: 'imports?jQuery=jquery'
  }
);

config.plugins.push(
  new ExtractTextPlugin('[name]-bundle.css', { allChunks: true }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoErrorsPlugin()
);

config.devtool = 'cheap-source-map';
config.debug = true;

console.log('Webpack dev build for Rails'); // eslint-disable-line no-console

module.exports = config;
