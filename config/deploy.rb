# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'happsyphoto'
set :cut_name_app, 'happsy'
set :repo_url, 'git@bitbucket.org:homlet/happsy_rails.git'
set :deploy_to, '/rest/u/apps/happsyphoto'
set :keep_releases, 5
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
set :linked_dirs, fetch(:linked_dirs) + %w{public/system public/uploads}

namespace :deploy do
  desc "Symlinks the database.yml"
  task :symlink_db do
    on roles :app do
      execute "cp #{release_path}/config/database.sample.yml #{release_path}/config/database.yml"
    end
  end

  namespace :unicorn do
    task :restart do
      on roles(:app), in: :sequence, wait: 5 do
        execute "sv restart #{fetch(:cut_name_app)}_unicorn"
      end
    end
  end

  task :tmp_clear do
    on release_roles :app do
      within release_path do
        with rails_env: fetch(:rails_env) do
          rake 'tmp:clear'
        end
      end
    end
  end

  task :webpack do
    on roles(:app) do
      webpack_path = "#{fetch(:release_path)}/app/assets/webpack"
      run_locally do
        # execute 'npm run build:clean --silent || true'
        execute 'npm run build:client'
      end
      execute "mkdir -p #{webpack_path}"
      Dir.glob('./app/assets/webpack/**/*').each do |file|
        upload! file, File.join(fetch(:release_path), file)
      end
    end
  end
end

before 'deploy:assets:precompile', 'deploy:symlink_db'
# before 'deploy:assets:precompile', 'deploy:tmp_clear'
before 'deploy:assets:precompile', 'deploy:webpack'
after "deploy:publishing", "deploy:unicorn:restart"