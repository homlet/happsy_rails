# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
# Add client/assets/ folders to asset pipeline's search path.
# If you do not want to move existing images and fonts from your Rails app
# you could also consider creating symlinks there that point to the original
# rails directories. In that case, you would not add these paths here.
Rails.application.config.assets.paths << Rails.root.join("client", "assets", "stylesheets")
Rails.application.config.assets.paths << Rails.root.join("client", "assets", "images")
Rails.application.config.assets.paths << Rails.root.join("client", "assets", "fonts")
Rails.application.config.assets.paths << Rails.root.join("app", "assets", "images")
Rails.application.config.assets.precompile += %w( web/application_non_webpack.css )
Rails.application.config.assets.precompile += %w( admin/application.js.js )
Rails.application.config.assets.precompile += %w( application_non_webpack.js )
Rails.application.config.assets.precompile += %w( web/application_static.css )
Rails.application.config.assets.precompile += %w( admin/application_non_webpack.css )
Rails.application.config.assets.precompile += %w( admin/application_static.css )
Rails.application.config.assets.precompile += %w( application_static.js )
Rails.application.config.assets.precompile += %w(ckeditor/* ckeditor/lang/*)
Rails.application.config.assets.precompile += %w( application_admin_static.js )
Rails.application.config.assets.precompile += %w( application_admin_non_webpack.js )