Refile.store = Refile::Backend::FileSystem.new(Rails.root.join("public/uploads/store").to_s)
Refile.cache = Refile::Backend::FileSystem.new(Rails.root.join("public/uploads/cache").to_s)
