Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  scope module: :web do
    root 'welcome#index'

    get '/uslugi', to: 'pages#servise'
    get '/about_me', to: 'pages#about_me'
    resources :categories, only: [:show]
    resources :galleries, only: [:show]
    resources :reviews, only: [:index]
    resources :pages, only: [:show]

    namespace :admin do
      root 'welcome#index'
      resource :session, only: [:new, :create, :destroy]
      resources :pictures
      resources :categories
      resources :calendars, only: [:index]
      resources :galleries, only: [:index, :new, :edit, :destroy]
      resources :reviews, only: [:index, :new, :create, :update, :edit, :destroy]
      resources :pages, only: [:index, :new, :create, :update, :edit, :destroy]
    end
  end

  namespace :api do
    namespace :admin do
      resources :pictures, only: [:create, :update, :destroy] do
        post 'save_favorite'
        post 'delete_favorite'
      end
      resources :calendars, only: [:create, :destroy]
      resources :galleries, only: [:create, :update, :destroy]
    end
  end
end
