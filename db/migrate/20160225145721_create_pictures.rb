class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :title
      t.string :publish_state, default: 'active'
      t.string :image_id

      t.timestamps null: false
    end
  end
end
