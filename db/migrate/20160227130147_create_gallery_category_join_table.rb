class CreateGalleryCategoryJoinTable < ActiveRecord::Migration
  def change
    create_join_table :galleries, :categories do |t|
      # t.index [:gallery_id, :category_id]
      # t.index [:category_id, :gallery_id]
    end
  end
end
