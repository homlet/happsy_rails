class DropGalleriesPictireJoinTable < ActiveRecord::Migration
  def change
    drop_join_table :categories, :galleries
  end
end
