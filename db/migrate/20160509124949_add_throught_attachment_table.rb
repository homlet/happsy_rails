class AddThroughtAttachmentTable < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.belongs_to :gallery, index: true
      t.belongs_to :picture, index: true
      t.integer :order
    end
  end
end
