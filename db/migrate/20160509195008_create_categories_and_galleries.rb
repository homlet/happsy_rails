class CreateCategoriesAndGalleries < ActiveRecord::Migration
  def change
    create_table :categories_galleries, id: false do |t|
      t.belongs_to :category, index: true
      t.belongs_to :gallery, index: true
    end
  end
end
