class AddSizeFilenameToPicture < ActiveRecord::Migration
  def change
    add_column :pictures, :size, :string
    add_column :pictures, :filename, :string
  end
end
