class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :title
      t.string :text
      t.string :image_id

      t.timestamps null: false
    end
  end
end
