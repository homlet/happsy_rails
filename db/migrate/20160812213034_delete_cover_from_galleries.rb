class DeleteCoverFromGalleries < ActiveRecord::Migration
  def change
    remove_column :galleries, :cover
  end
end
